package com.abcfireperu.app.management.service.fireextinguishertypes;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisherType;
import com.abcfireperu.app.management.repository.FireExtinguisherTypeRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherTypeFindByIdService {

  @Autowired
  private FireExtinguisherTypeRepository fireExtinguisherTypeRepository;

  public Optional<FireExtinguisherType> getFireExtinguisherTypeById(Long Id)
      throws ResourceNotFoundExceptionClass {
    Optional<FireExtinguisherType> fireExtinguisherType = Optional
        .ofNullable(fireExtinguisherTypeRepository.findById(Id)
            .orElseThrow(() -> new ResourceNotFoundExceptionClass(
                "FireExtinguisherBrand not found for this id :: " + Id)));
    return fireExtinguisherType;
  }

}
