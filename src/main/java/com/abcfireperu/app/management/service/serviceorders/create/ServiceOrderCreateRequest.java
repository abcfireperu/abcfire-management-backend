package com.abcfireperu.app.management.service.serviceorders.create;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceOrderCreateRequest {

  private Long idClient;
  private String code;
}
