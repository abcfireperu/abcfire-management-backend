package com.abcfireperu.app.management.service.serviceorders;

import com.abcfireperu.app.management.model.ClientType;
import com.abcfireperu.app.management.model.ServiceOrder;
import com.abcfireperu.app.management.model.ServiceOrderDetail;
import com.abcfireperu.app.management.repository.ServiceOrderDetailRepository;
import com.abcfireperu.app.management.repository.ServiceOrderFireExtinguisherRepository;
import com.abcfireperu.app.management.repository.ServiceOrderRepository;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherBrandResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherTypeResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ClientResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceOrderDetailResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceOrderResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceResponse;
import com.abcfireperu.app.management.service.serviceorders.getbypage.ServiceOrderPageResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ServiceOrderFindByPageService {

  @Autowired
  private ServiceOrderRepository serviceOrderRepository;

  @Autowired
  private ServiceOrderDetailRepository serviceOrderDetailRepository;

  @Autowired
  private ServiceOrderFireExtinguisherRepository serviceOrderFireExtinguisherRepository;

  public ServiceOrderPageResponse getServiceOrderByPage(int pageNumber, int pageSize) {

    ServiceOrderPageResponse pageResponse = new ServiceOrderPageResponse();
    List<ServiceOrder> serviceOrderIterable = new ArrayList<>();
    List<ServiceOrderResponse> serviceOrderResponseList = new ArrayList<>();

    Pageable paging = PageRequest.of(pageNumber, pageSize, Sort.by("Id"));
    Page<ServiceOrder> serviceOrderPage = serviceOrderRepository.findAll(paging);
    serviceOrderIterable = serviceOrderPage.getContent();

    for (ServiceOrder s : serviceOrderIterable) {
      ServiceOrderResponse serviceOrderResponse = new ServiceOrderResponse();
      ClientResponse clientResponse = new ClientResponse();
      List<ServiceOrderDetail> serviceOrderDetailList = new ArrayList<>();
      List<ServiceOrderDetailResponse> serviceOrderDetailsResponseList = new ArrayList<>();

      clientResponse.setAddress(s.getClient().getAddress());
      clientResponse.setCode(s.getClient().getCode());
      clientResponse.setContact(s.getClient().getContact());
      clientResponse.setCountry(s.getClient().getCountry());
      clientResponse.setId(s.getClient().getId());
      clientResponse.setName(s.getClient().getName());
      clientResponse.setDocumentType(s.getClient().getDocumentType());
      clientResponse.setManager(s.getClient().getManager());
      clientResponse.setMail(s.getClient().getMail());
      clientResponse.setRuc(s.getClient().getRuc());
      clientResponse.setPhoneNumber(s.getClient().getPhoneNumber());
      if (s.getClient().getType() == ClientType.NATURAL) {
        clientResponse.setType("NATURAL");
      } else {
        clientResponse.setType("JURIDICA");
      }

      for (ServiceOrderDetail so : s.getServiceOrderDetails()) {
        ServiceOrderDetailResponse serviceOrderDetailResponse = new ServiceOrderDetailResponse();
        ServiceResponse serviceResponse = new ServiceResponse();
        FireExtinguisherResponse fireExtinguisherResponse = new FireExtinguisherResponse();
        FireExtinguisherBrandResponse fireExtinguisherBrandResponse = new FireExtinguisherBrandResponse();
        FireExtinguisherTypeResponse fireExtinguisherTypeResponse = new FireExtinguisherTypeResponse();

        fireExtinguisherBrandResponse
            .setName(so.getFireExtinguisher().getFireExtinguisherBrand().getName());
        fireExtinguisherBrandResponse
            .setCode(so.getFireExtinguisher().getFireExtinguisherBrand().getCode());
        fireExtinguisherBrandResponse
            .setId(so.getFireExtinguisher().getFireExtinguisherBrand().getId());

        fireExtinguisherTypeResponse
            .setName(so.getFireExtinguisher().getFireExtinguisherType().getName());
        fireExtinguisherTypeResponse
            .setCode(so.getFireExtinguisher().getFireExtinguisherType().getCode());
        fireExtinguisherTypeResponse
            .setId(so.getFireExtinguisher().getFireExtinguisherType().getId());

        fireExtinguisherResponse.setUUID(so.getFireExtinguisher().getUUID());
        fireExtinguisherResponse.setWarrantyDate(so.getFireExtinguisher().getWarrantyDate());
        fireExtinguisherResponse.setFireExtinguisherType(fireExtinguisherTypeResponse);
        fireExtinguisherResponse.setFireExtinguisherBrand(fireExtinguisherBrandResponse);
        fireExtinguisherResponse.setModel(so.getFireExtinguisher().getModel());
        fireExtinguisherResponse.setSerialNumber(so.getFireExtinguisher().getSerialNumber());
        fireExtinguisherResponse
            .setCapacityQuantity(so.getFireExtinguisher().getCapacityQuantity());
        fireExtinguisherResponse.setCapacityUnit(so.getFireExtinguisher().getCapacityUnit());
        fireExtinguisherResponse
            .setManufactoringDate(so.getFireExtinguisher().getManufactoringDate());
        fireExtinguisherResponse
            .setLastHydrostaticTest(so.getFireExtinguisher().getLastHydrostaticTest());

        serviceResponse.setCode(so.getServiceModel().getCode());
        serviceResponse.setName(so.getServiceModel().getName());
        serviceResponse.setId(so.getServiceModel().getId());

        serviceOrderDetailResponse.setId(so.getId());
        serviceOrderDetailResponse.setHydrostaticTest(so.getHydrostaticTest());
        serviceOrderDetailResponse.setLastMaintenance(so.getLastMaintenance());
        serviceOrderDetailResponse.setService(serviceResponse);

        serviceOrderDetailsResponseList.add(serviceOrderDetailResponse);

      }

      serviceOrderResponse.setStatus(s.getStatus());
      serviceOrderResponse.setId(s.getId());
      serviceOrderResponse.setCode(s.getCode());
      serviceOrderResponse.setClient(clientResponse);
      serviceOrderResponse.setServiceOrderDetails(serviceOrderDetailsResponseList);
    }

    pageResponse.setPageSize(pageSize);
    pageResponse.setPageNumber(pageNumber);
    pageResponse.setServiceOrderResponseList(serviceOrderResponseList);
    pageResponse.setPageTotal(serviceOrderPage.getTotalPages());

    return pageResponse;
  }

}
