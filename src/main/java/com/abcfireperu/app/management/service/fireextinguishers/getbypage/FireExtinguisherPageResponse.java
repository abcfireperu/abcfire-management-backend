package com.abcfireperu.app.management.service.fireextinguishers.getbypage;

import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FireExtinguisherPageResponse {

  private int pageNumber;
  private int pageSize;
  private int pageTotal;
  private List<FireExtinguisherResponse> fireExtinguisherList;

}
