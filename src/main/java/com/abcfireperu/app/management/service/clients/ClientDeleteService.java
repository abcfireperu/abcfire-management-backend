package com.abcfireperu.app.management.service.clients;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Client;
import com.abcfireperu.app.management.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientDeleteService {

  @Autowired
  private ClientRepository clientRepository;

  public String deleteClient(Long Id) throws ResourceNotFoundExceptionClass {
    Client client = clientRepository.findById(Id)
        .orElseThrow(
            () -> new ResourceNotFoundExceptionClass("Client not found for this id :: " + Id));
    clientRepository.delete(client);
    return "Ready";
  }

}
