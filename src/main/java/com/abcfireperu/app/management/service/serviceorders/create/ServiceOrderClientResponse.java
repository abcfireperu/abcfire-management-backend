package com.abcfireperu.app.management.service.serviceorders.create;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceOrderClientResponse {

  private Long id;
  private String code;
  private String name;
  private String type;
  private String documentType;
  private String ruc;
  private String address;
  private String country;
  private String phoneNumber;
  private String mail;
  private String contact;
  private String manager;
}
