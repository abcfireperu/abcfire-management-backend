package com.abcfireperu.app.management.service.service.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceRequest {

  private String code;
  private String name;

}
