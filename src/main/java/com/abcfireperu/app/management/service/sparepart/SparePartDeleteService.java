package com.abcfireperu.app.management.service.sparepart;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.SparePart;
import com.abcfireperu.app.management.repository.SparePartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SparePartDeleteService {

  @Autowired
  private SparePartRepository sparePartRepository;

  public String deleteSparePart(Long Id) throws ResourceNotFoundExceptionClass {
    SparePart sparePart = sparePartRepository.findById(Id)
        .orElseThrow(() -> new ResourceNotFoundExceptionClass(
            "Spare Part not found for this id :: " + Id));
    sparePartRepository.delete(sparePart);
    return "Ready";
  }

}
