package com.abcfireperu.app.management.service.fireextinguishers.generic;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FireExtinguisherResponse {

  private String UUID;
  private Date warrantyDate;
  private FireExtinguisherBrandResponse fireExtinguisherBrand;
  private String model;
  private String serialNumber;
  private FireExtinguisherTypeResponse fireExtinguisherType;
  private float capacityQuantity;
  private String capacityUnit;
  private Date manufactoringDate;
  private Date lastHydrostaticTest;
}
