package com.abcfireperu.app.management.service.serviceorders;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.ClientType;
import com.abcfireperu.app.management.model.ServiceOrder;
import com.abcfireperu.app.management.model.ServiceOrderDetail;
import com.abcfireperu.app.management.repository.ServiceOrderDetailRepository;
import com.abcfireperu.app.management.repository.ServiceOrderFireExtinguisherRepository;
import com.abcfireperu.app.management.repository.ServiceOrderRepository;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherBrandResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherTypeResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ClientResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceOrderDetailResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceOrderResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceOrderFindByIdService {

  @Autowired
  private ServiceOrderRepository serviceOrderRepository;

  @Autowired
  private ServiceOrderDetailRepository serviceOrderDetailRepository;

  @Autowired
  private ServiceOrderFireExtinguisherRepository serviceOrderFireExtinguisherRepository;

  public ServiceOrderResponse getServiceOrderById(Long Id) throws ResourceNotFoundExceptionClass {

    ServiceOrderResponse serviceOrderResponse = new ServiceOrderResponse();
    ClientResponse clientResponse = new ClientResponse();
    List<ServiceOrderDetailResponse> serviceOrderDetailsResponseList = new ArrayList<>();

    Optional<ServiceOrder> serviceOrderOptional = Optional
        .ofNullable(serviceOrderRepository.findById(Id)
            .orElseThrow(() -> new ResourceNotFoundExceptionClass(
                "Service Order not found by this id ::" + Id)));

    clientResponse.setAddress(serviceOrderOptional.get().getClient().getAddress());
    clientResponse.setCode(serviceOrderOptional.get().getClient().getCode());
    clientResponse.setContact(serviceOrderOptional.get().getClient().getContact());
    clientResponse.setCountry(serviceOrderOptional.get().getClient().getCountry());
    clientResponse.setId(serviceOrderOptional.get().getClient().getId());
    clientResponse.setName(serviceOrderOptional.get().getClient().getName());
    clientResponse.setDocumentType(serviceOrderOptional.get().getClient().getDocumentType());
    clientResponse.setManager(serviceOrderOptional.get().getClient().getManager());
    clientResponse.setMail(serviceOrderOptional.get().getClient().getMail());
    clientResponse.setRuc(serviceOrderOptional.get().getClient().getRuc());
    clientResponse.setPhoneNumber(serviceOrderOptional.get().getClient().getPhoneNumber());
    if (serviceOrderOptional.get().getClient().getType() == ClientType.NATURAL) {
      clientResponse.setType("NATURAL");
    } else {
      clientResponse.setType("JURIDICA");
    }

    for (ServiceOrderDetail so : serviceOrderOptional.get().getServiceOrderDetails()) {
      ServiceOrderDetailResponse serviceOrderDetailResponse = new ServiceOrderDetailResponse();
      ServiceResponse serviceResponse = new ServiceResponse();
      FireExtinguisherResponse fireExtinguisherResponse = new FireExtinguisherResponse();
      FireExtinguisherBrandResponse fireExtinguisherBrandResponse = new FireExtinguisherBrandResponse();
      FireExtinguisherTypeResponse fireExtinguisherTypeResponse = new FireExtinguisherTypeResponse();

      fireExtinguisherBrandResponse
          .setName(so.getFireExtinguisher().getFireExtinguisherBrand().getName());
      fireExtinguisherBrandResponse
          .setCode(so.getFireExtinguisher().getFireExtinguisherBrand().getCode());
      fireExtinguisherBrandResponse
          .setId(so.getFireExtinguisher().getFireExtinguisherBrand().getId());

      fireExtinguisherTypeResponse
          .setName(so.getFireExtinguisher().getFireExtinguisherType().getName());
      fireExtinguisherTypeResponse
          .setCode(so.getFireExtinguisher().getFireExtinguisherType().getCode());
      fireExtinguisherTypeResponse
          .setId(so.getFireExtinguisher().getFireExtinguisherType().getId());

      fireExtinguisherResponse.setUUID(so.getFireExtinguisher().getUUID());
      fireExtinguisherResponse.setWarrantyDate(so.getFireExtinguisher().getWarrantyDate());
      fireExtinguisherResponse.setFireExtinguisherType(fireExtinguisherTypeResponse);
      fireExtinguisherResponse.setFireExtinguisherBrand(fireExtinguisherBrandResponse);
      fireExtinguisherResponse.setModel(so.getFireExtinguisher().getModel());
      fireExtinguisherResponse.setSerialNumber(so.getFireExtinguisher().getSerialNumber());
      fireExtinguisherResponse
          .setCapacityQuantity(so.getFireExtinguisher().getCapacityQuantity());
      fireExtinguisherResponse.setCapacityUnit(so.getFireExtinguisher().getCapacityUnit());
      fireExtinguisherResponse
          .setManufactoringDate(so.getFireExtinguisher().getManufactoringDate());
      fireExtinguisherResponse
          .setLastHydrostaticTest(so.getFireExtinguisher().getLastHydrostaticTest());

      serviceResponse.setCode(so.getServiceModel().getCode());
      serviceResponse.setName(so.getServiceModel().getName());
      serviceResponse.setId(so.getServiceModel().getId());

      serviceOrderDetailResponse.setId(so.getId());
      serviceOrderDetailResponse.setHydrostaticTest(so.getHydrostaticTest());
      serviceOrderDetailResponse.setLastMaintenance(so.getLastMaintenance());
      serviceOrderDetailResponse.setService(serviceResponse);

      serviceOrderDetailsResponseList.add(serviceOrderDetailResponse);

    }
    serviceOrderResponse.setStatus(serviceOrderOptional.get().getStatus());
    serviceOrderResponse.setId(serviceOrderOptional.get().getId());
    serviceOrderResponse.setCode(serviceOrderOptional.get().getCode());
    serviceOrderResponse.setClient(clientResponse);
    serviceOrderResponse.setServiceOrderDetails(serviceOrderDetailsResponseList);

    return serviceOrderResponse;
  }

}
