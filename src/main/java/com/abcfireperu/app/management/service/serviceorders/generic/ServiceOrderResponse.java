package com.abcfireperu.app.management.service.serviceorders.generic;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceOrderResponse {

  private Long id;
  private String code;
  private ClientResponse client;
  private List<ServiceOrderDetailResponse> serviceOrderDetails;
  private String status;

}
