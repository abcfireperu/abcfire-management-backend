package com.abcfireperu.app.management.service.fireextinguishers.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FireExtinguisherTypeResponse {

  private Long id;
  private String code;
  private String name;

}
