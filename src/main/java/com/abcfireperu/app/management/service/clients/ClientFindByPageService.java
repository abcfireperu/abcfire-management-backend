package com.abcfireperu.app.management.service.clients;

import com.abcfireperu.app.management.model.Client;
import com.abcfireperu.app.management.model.ClientType;
import com.abcfireperu.app.management.model.Zone;
import com.abcfireperu.app.management.repository.ClientRepository;
import com.abcfireperu.app.management.service.clients.generic.ClientResponse;
import com.abcfireperu.app.management.service.clients.generic.ZoneResponse;
import com.abcfireperu.app.management.service.clients.getbypage.ClientPageResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ClientFindByPageService {

  @Autowired
  ClientRepository clientRepository;

  public ClientPageResponse getClientByPage(int pageNumber, int pageSize) {
    ClientPageResponse clientPageResponse = new ClientPageResponse();
    List<ClientResponse> clientListResponse = new ArrayList<>();

    Pageable paging = PageRequest.of(pageNumber, pageSize, Sort.by("Id"));
    Page<Client> clientPage = clientRepository.findAll(paging);
    List<Client> clientList = clientPage.getContent();

    for (Client c : clientList) {
      ClientResponse clientResponse = new ClientResponse();
      List<ZoneResponse> zoneResponseList = new ArrayList<>();

      for (Zone z : c.getZones()) {
        ZoneResponse zoneResponse = new ZoneResponse();
        zoneResponse.setCode(z.getCode());
        zoneResponse.setName(z.getName());
        zoneResponse.setId(z.getId());
        zoneResponseList.add(zoneResponse);
      }

      clientResponse.setAddress(c.getAddress());
      clientResponse.setCode(c.getCode());
      clientResponse.setContact(c.getContact());
      clientResponse.setCountry(c.getCountry());
      clientResponse.setId(c.getId());
      clientResponse.setName(c.getName());
      clientResponse.setDocumentType(c.getDocumentType());
      clientResponse.setManager(c.getManager());
      clientResponse.setMail(c.getMail());
      clientResponse.setRuc(c.getRuc());
      clientResponse.setZones(zoneResponseList);
      clientResponse.setPhoneNumber(c.getPhoneNumber());

      if (c.getType() == ClientType.NATURAL) {
        clientResponse.setType("NATURAL");
      } else {
        clientResponse.setType("JURIDICA");
      }

      clientListResponse.add(clientResponse);

    }

    clientPageResponse.setPageSize(pageSize);
    clientPageResponse.setPageNumber(pageNumber);
    clientPageResponse.setClientResponseList(clientListResponse);
    clientPageResponse.setPageTotal(clientPage.getTotalPages());

    return clientPageResponse;

  }
}
