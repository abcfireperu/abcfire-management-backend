package com.abcfireperu.app.management.service.service;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.ServiceModel;
import com.abcfireperu.app.management.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceDeleteService {

  @Autowired
  private ServiceRepository serviceRepository;

  public String deleteService(Long Id) throws ResourceNotFoundExceptionClass {

    ServiceModel service = serviceRepository.findById(Id)
        .orElseThrow(() -> new ResourceNotFoundExceptionClass(
            "Service not found for this id :: " + Id));
    serviceRepository.delete(service);
    return "Ready";

  }


}
