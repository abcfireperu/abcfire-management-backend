package com.abcfireperu.app.management.service.serviceorders.create;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceOrderCreateResponse {

  private Long id;
  private String code;
  private ServiceOrderClientResponse client;

}
