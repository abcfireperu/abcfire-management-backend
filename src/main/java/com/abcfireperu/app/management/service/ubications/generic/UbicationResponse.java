package com.abcfireperu.app.management.service.ubications.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UbicationResponse {

  private Long id;
  private String name;
  private String type;
  private String ubigeo;
  private UbicationResponse parent;

}
