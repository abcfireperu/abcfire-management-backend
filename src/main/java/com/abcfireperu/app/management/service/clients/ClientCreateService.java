package com.abcfireperu.app.management.service.clients;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Client;
import com.abcfireperu.app.management.model.ClientType;
import com.abcfireperu.app.management.model.Zone;
import com.abcfireperu.app.management.repository.ClientRepository;
import com.abcfireperu.app.management.repository.ZoneRepository;
import com.abcfireperu.app.management.service.clients.generic.ClientRequest;
import com.abcfireperu.app.management.service.clients.generic.ClientResponse;
import com.abcfireperu.app.management.service.clients.generic.ZoneResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientCreateService {

  @Autowired
  private ClientRepository clientRepository;

  @Autowired
  private ZoneRepository zoneRepository;

  public ClientResponse createClient(ClientRequest clientRequest)
      throws ResourceNotFoundExceptionClass {

    Client client = new Client();
    ClientResponse clientResponse = new ClientResponse();
    Set<Zone> zoneList = new HashSet<>();
    List<ZoneResponse> zoneResponseList = new ArrayList<>();

    for (Long id : clientRequest.getZoneIdList()) {
      ZoneResponse zoneResponse = new ZoneResponse();
      Optional<Zone> optionalZone = zoneRepository.findById(id);
      if (optionalZone.get() == null) {
        throw new ResourceNotFoundExceptionClass("Zone not found for this id :: " + id);
      } else {
        zoneList.add(optionalZone.get());

        zoneResponse.setCode(optionalZone.get().getCode());
        zoneResponse.setId(optionalZone.get().getId());
        zoneResponse.setName(optionalZone.get().getName());
        zoneResponseList.add(zoneResponse);
      }
    }

    client.setAddress(clientRequest.getAddress());
    client.setCode(clientRequest.getCode());
    client.setContact(clientRequest.getContact());
    client.setCountry(clientRequest.getCountry());
    client.setId(clientRequest.getId());
    client.setName(clientRequest.getName());
    client.setManager(clientRequest.getManager());
    client.setMail(clientRequest.getMail());
    client.setRuc(clientRequest.getRuc());
    client.setDocumentType(clientRequest.getDocumentType());
    client.setPhoneNumber(clientRequest.getPhoneNumber());
    if (clientRequest.getType() == "NATURAL") {
      client.setType(ClientType.NATURAL);
    } else {
      client.setType(ClientType.JURIDICA);
    }
    client.setZones(zoneList);

    for (Long id : clientRequest.getZoneIdList()) {
      ZoneResponse zoneResponse = new ZoneResponse();
      Zone zone = zoneRepository.findById(id).get();
      zone.getClients().add(client);
    }

    clientRepository.save(client);

    clientResponse.setAddress(clientRequest.getAddress());
    clientResponse.setCode(clientRequest.getCode());
    clientResponse.setContact(clientRequest.getContact());
    clientResponse.setCountry(clientRequest.getCountry());
    clientResponse.setId(clientRequest.getId());
    clientResponse.setName(clientRequest.getName());
    clientResponse.setManager(clientRequest.getManager());
    clientResponse.setMail(clientRequest.getMail());
    clientResponse.setRuc(clientRequest.getRuc());
    clientResponse.setDocumentType(clientRequest.getDocumentType());
    clientResponse.setPhoneNumber(clientRequest.getPhoneNumber());
    clientResponse.setType(clientRequest.getType());
    clientResponse.setZones(zoneResponseList);

    return clientResponse;
  }


}
