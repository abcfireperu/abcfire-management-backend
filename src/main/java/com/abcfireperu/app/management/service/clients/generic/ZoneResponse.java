package com.abcfireperu.app.management.service.clients.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ZoneResponse {

  private Long id;
  private String name;
  private String code;
}
