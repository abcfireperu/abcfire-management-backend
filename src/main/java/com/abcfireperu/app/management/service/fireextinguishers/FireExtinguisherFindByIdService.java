package com.abcfireperu.app.management.service.fireextinguishers;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisher;
import com.abcfireperu.app.management.repository.FireExtinguisherRepository;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherBrandResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherTypeResponse;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherFindByIdService {

  @Autowired
  private FireExtinguisherRepository fireExtinguisherRepository;

  public FireExtinguisherResponse getFireExtinguisherById(Long Id)
      throws ResourceNotFoundExceptionClass {

    FireExtinguisherResponse fireExtinguisherResponse = new FireExtinguisherResponse();
    FireExtinguisherBrandResponse fireExtinguisherBrandResponse = new FireExtinguisherBrandResponse();
    FireExtinguisherTypeResponse fireExtinguisherTypeResponse = new FireExtinguisherTypeResponse();

    Optional<FireExtinguisher> fireExtinguisherOptional = Optional
        .ofNullable(fireExtinguisherRepository.findById(Id).orElseThrow(
            () -> new ResourceNotFoundExceptionClass(
                "FireExtinguisher not found by this id ::" + Id)));

    fireExtinguisherBrandResponse
        .setName(fireExtinguisherOptional.get().getFireExtinguisherBrand().getName());
    fireExtinguisherBrandResponse
        .setCode(fireExtinguisherOptional.get().getFireExtinguisherBrand().getCode());
    fireExtinguisherBrandResponse
        .setId(fireExtinguisherOptional.get().getFireExtinguisherBrand().getId());

    fireExtinguisherTypeResponse
        .setName(fireExtinguisherOptional.get().getFireExtinguisherType().getName());
    fireExtinguisherTypeResponse
        .setCode(fireExtinguisherOptional.get().getFireExtinguisherType().getCode());
    fireExtinguisherTypeResponse
        .setId(fireExtinguisherOptional.get().getFireExtinguisherType().getId());

    fireExtinguisherResponse.setUUID(fireExtinguisherOptional.get().getUUID());
    fireExtinguisherResponse.setWarrantyDate(fireExtinguisherOptional.get().getWarrantyDate());
    fireExtinguisherResponse.setFireExtinguisherType(fireExtinguisherTypeResponse);
    fireExtinguisherResponse.setFireExtinguisherBrand(fireExtinguisherBrandResponse);
    fireExtinguisherResponse.setModel(fireExtinguisherOptional.get().getModel());
    fireExtinguisherResponse.setSerialNumber(fireExtinguisherOptional.get().getSerialNumber());
    fireExtinguisherResponse
        .setCapacityQuantity(fireExtinguisherOptional.get().getCapacityQuantity());
    fireExtinguisherResponse.setCapacityUnit(fireExtinguisherOptional.get().getCapacityUnit());
    fireExtinguisherResponse
        .setManufactoringDate(fireExtinguisherOptional.get().getManufactoringDate());
    fireExtinguisherResponse
        .setLastHydrostaticTest(fireExtinguisherOptional.get().getLastHydrostaticTest());

    return fireExtinguisherResponse;
  }


}
