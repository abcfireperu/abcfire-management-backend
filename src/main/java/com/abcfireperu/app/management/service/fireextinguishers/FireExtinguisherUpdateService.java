package com.abcfireperu.app.management.service.fireextinguishers;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisher;
import com.abcfireperu.app.management.model.FireExtinguisherBrand;
import com.abcfireperu.app.management.model.FireExtinguisherType;
import com.abcfireperu.app.management.repository.FireExtinguisherBrandRepository;
import com.abcfireperu.app.management.repository.FireExtinguisherRepository;
import com.abcfireperu.app.management.repository.FireExtinguisherTypeRepository;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherBrandResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherRequest;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherTypeResponse;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherUpdateService {

  @Autowired
  private FireExtinguisherRepository fireExtinguisherRepository;

  @Autowired
  private FireExtinguisherBrandRepository fireExtinguisherBrandRepository;

  @Autowired
  private FireExtinguisherTypeRepository fireExtinguisherTypeRepository;

  public FireExtinguisherResponse updateFireExtinguisher(Long Id,
      FireExtinguisherRequest fireExtinguisherRequest) throws ResourceNotFoundExceptionClass {

    FireExtinguisher fireExtinguisher = fireExtinguisherRepository
        .findById(Id)
        .orElseThrow(
            () -> new ResourceNotFoundExceptionClass(
                "FireExtinguisher not found for this id :: " + Id));

    Optional<FireExtinguisherBrand> fireExtinguisherBrandOptional = fireExtinguisherBrandRepository
        .findById(fireExtinguisherRequest.getBrandId());
    if (fireExtinguisherBrandOptional == null) {
      throw new ResourceNotFoundExceptionClass(
          "FireExtinguisherBrand not found for this id :: " + fireExtinguisherRequest.getBrandId());
    }

    Optional<FireExtinguisherType> fireExtinguisherTypeOptional = fireExtinguisherTypeRepository
        .findById(
            fireExtinguisherRequest.getTypeId());
    if (fireExtinguisherTypeOptional == null) {
      throw new ResourceNotFoundExceptionClass(
          "FireExtinguisherType not found for this id :: " + fireExtinguisherRequest.getTypeId());
    }

    fireExtinguisher.setUUID(fireExtinguisherRequest.getUUID());
    fireExtinguisher.setWarrantyDate(fireExtinguisherRequest.getWarrantyDate());
    fireExtinguisher.setFireExtinguisherType(fireExtinguisherTypeOptional.get());
    fireExtinguisher.setFireExtinguisherBrand(fireExtinguisherBrandOptional.get());
    fireExtinguisher.setModel(fireExtinguisherRequest.getModel());
    fireExtinguisher.setSerialNumber(fireExtinguisherRequest.getSerialNumber());
    fireExtinguisher.setCapacityQuantity(fireExtinguisherRequest.getCapacityQuantity());
    fireExtinguisher.setCapacityUnit(fireExtinguisherRequest.getCapacityUnit());
    fireExtinguisher.setManufactoringDate(fireExtinguisherRequest.getManufactoringDate());
    fireExtinguisher.setLastHydrostaticTest(fireExtinguisherRequest.getLastHydrostaticTest());
    final FireExtinguisher updatedFireExtinguisher = fireExtinguisherRepository
        .save(fireExtinguisher);

    FireExtinguisherResponse fireExtinguisherResponse = new FireExtinguisherResponse();
    FireExtinguisherTypeResponse fireExtinguisherTypeResponse = new FireExtinguisherTypeResponse();
    FireExtinguisherBrandResponse fireExtinguisherBrandResponse = new FireExtinguisherBrandResponse();

    fireExtinguisherTypeResponse.setCode(fireExtinguisherTypeOptional.get().getCode());
    fireExtinguisherTypeResponse.setId(fireExtinguisherTypeOptional.get().getId());
    fireExtinguisherTypeResponse.setName(fireExtinguisherTypeOptional.get().getName());

    fireExtinguisherBrandResponse.setCode(fireExtinguisherBrandOptional.get().getCode());
    fireExtinguisherBrandResponse.setId(fireExtinguisherBrandOptional.get().getId());
    fireExtinguisherBrandResponse.setName(fireExtinguisherBrandOptional.get().getName());

    fireExtinguisherResponse.setUUID(fireExtinguisher.getUUID());
    fireExtinguisherResponse.setWarrantyDate(fireExtinguisher.getWarrantyDate());
    fireExtinguisherResponse.setFireExtinguisherBrand(fireExtinguisherBrandResponse);
    fireExtinguisherResponse.setFireExtinguisherType(fireExtinguisherTypeResponse);

    fireExtinguisherResponse.setModel(fireExtinguisher.getModel());
    fireExtinguisherResponse.setSerialNumber(fireExtinguisher.getSerialNumber());
    fireExtinguisherResponse.setCapacityQuantity(fireExtinguisher.getCapacityQuantity());
    fireExtinguisherResponse.setCapacityUnit(fireExtinguisher.getCapacityUnit());
    fireExtinguisherResponse.setManufactoringDate(fireExtinguisher.getManufactoringDate());
    fireExtinguisherResponse.setLastHydrostaticTest(fireExtinguisher.getLastHydrostaticTest());

    return fireExtinguisherResponse;

  }

}
