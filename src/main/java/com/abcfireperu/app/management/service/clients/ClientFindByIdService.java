package com.abcfireperu.app.management.service.clients;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Client;
import com.abcfireperu.app.management.model.ClientType;
import com.abcfireperu.app.management.model.Zone;
import com.abcfireperu.app.management.repository.ClientRepository;
import com.abcfireperu.app.management.service.clients.generic.ClientResponse;
import com.abcfireperu.app.management.service.clients.generic.ZoneResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientFindByIdService {

  @Autowired
  private ClientRepository clientRepository;

  public ClientResponse getClientById(Long Id) throws ResourceNotFoundExceptionClass {

    ClientResponse clientResponse = new ClientResponse();
    List<ZoneResponse> zoneResponseList = new ArrayList<>();

    Optional<Client> clientOptional = Optional.ofNullable(clientRepository.findById(Id).orElseThrow(
        () -> new ResourceNotFoundExceptionClass("FireExtinguisher not found by this id ::" + Id)));

    for (Zone z : clientOptional.get().getZones()) {
      ZoneResponse zoneResponse = new ZoneResponse();
      zoneResponse.setCode(z.getCode());
      zoneResponse.setName(z.getName());
      zoneResponse.setId(z.getId());
      zoneResponseList.add(zoneResponse);
    }

    clientResponse.setAddress(clientOptional.get().getAddress());
    clientResponse.setCode(clientOptional.get().getCode());
    clientResponse.setContact(clientOptional.get().getContact());
    clientResponse.setCountry(clientOptional.get().getCountry());
    clientResponse.setId(clientOptional.get().getId());
    clientResponse.setName(clientOptional.get().getName());
    clientResponse.setDocumentType(clientOptional.get().getDocumentType());
    clientResponse.setManager(clientOptional.get().getManager());
    clientResponse.setMail(clientOptional.get().getMail());
    clientResponse.setRuc(clientOptional.get().getRuc());
    clientResponse.setZones(zoneResponseList);
    clientResponse.setPhoneNumber(clientOptional.get().getPhoneNumber());

    if (clientOptional.get().getType() == ClientType.NATURAL) {
      clientResponse.setType("NATURAL");
    } else {
      clientResponse.setType("JURIDICA");
    }

    return clientResponse;
  }

}
