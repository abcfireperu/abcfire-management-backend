package com.abcfireperu.app.management.service.ubications;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Ubication;
import com.abcfireperu.app.management.repository.UbicationRepository;
import com.abcfireperu.app.management.service.ubications.generic.UbicationResponse;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UbicationFindByNameAndTypeService {

  @Autowired
  private UbicationRepository ubicationRepository;

  public UbicationResponse getUbicationByNameAndType(String name, String type)
      throws ResourceNotFoundExceptionClass {

    UbicationResponse ubicationResponse = new UbicationResponse();
    int c = 0;

    Optional<Ubication> ubicationOptional = Optional
        .ofNullable(ubicationRepository.findByNameAndType(name, type)
            .orElseThrow(() -> new ResourceNotFoundExceptionClass(
                "Ubication not found by this name and type ::" + name + ", " + type)));
    Ubication ubicationTemp = ubicationOptional.get();

    ubicationResponse.setName(ubicationTemp.getName());
    ubicationResponse.setType(ubicationTemp.getType());
    ubicationResponse.setId(ubicationTemp.getId());
    ubicationResponse.setUbigeo(ubicationTemp.getUbigeo());

    UbicationResponse ubicationResponseIterator = new UbicationResponse();
    while (ubicationTemp.getParent() != null) {

      Ubication ubicationIterator = new Ubication();
      ubicationIterator = ubicationTemp.getParent();
      System.out.println(ubicationTemp.getName());
      UbicationResponse ubicationResponseTemp = new UbicationResponse();

      ubicationResponseTemp.setName(ubicationIterator.getName());
      ubicationResponseTemp.setType(ubicationIterator.getType());
      ubicationResponseTemp.setId(ubicationIterator.getId());
      ubicationResponseTemp.setUbigeo(ubicationIterator.getUbigeo());

      if (c == 0) {

        ubicationResponse.setParent(ubicationResponseTemp);

      } else {
        ubicationResponseTemp.setParent(ubicationResponseIterator);
      }
      ubicationResponseIterator = ubicationResponseTemp;
      ubicationTemp = ubicationIterator;
      c++;

    }

    return ubicationResponse;
  }
}
