package com.abcfireperu.app.management.service.clients.getbypage;

import com.abcfireperu.app.management.service.clients.generic.ClientResponse;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientPageResponse {

  private int pageNumber;
  private int pageSize;
  private int pageTotal;
  private List<ClientResponse> clientResponseList;
}
