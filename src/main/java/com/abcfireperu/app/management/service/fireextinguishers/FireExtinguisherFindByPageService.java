package com.abcfireperu.app.management.service.fireextinguishers;

import com.abcfireperu.app.management.model.FireExtinguisher;
import com.abcfireperu.app.management.repository.FireExtinguisherRepository;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherBrandResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherTypeResponse;
import com.abcfireperu.app.management.service.fireextinguishers.getbypage.FireExtinguisherPageResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherFindByPageService {

  @Autowired
  FireExtinguisherRepository fireExtinguisherRepository;

  public FireExtinguisherPageResponse getFireExtinguisherByPage(int pageNumber, int pageSize) {
    FireExtinguisherPageResponse pageResponse = new FireExtinguisherPageResponse();
    List<FireExtinguisher> fireExtinguisherIterable = new ArrayList<>();
    List<FireExtinguisherResponse> fireExtinguisherListResponse = new ArrayList<>();

    Pageable paging = PageRequest.of(pageNumber, pageSize, Sort.by("Id"));
    Page<FireExtinguisher> fireExtinguisherPage = fireExtinguisherRepository.findAll(paging);
    fireExtinguisherIterable = fireExtinguisherPage.getContent();

    for (FireExtinguisher fe : fireExtinguisherIterable) {
      FireExtinguisherResponse fireExtinguisherResponse = new FireExtinguisherResponse();
      FireExtinguisherBrandResponse fireExtinguisherBrandResponse = new FireExtinguisherBrandResponse();
      FireExtinguisherTypeResponse fireExtinguisherTypeResponse = new FireExtinguisherTypeResponse();

      fireExtinguisherBrandResponse.setName(fe.getFireExtinguisherBrand().getName());
      fireExtinguisherBrandResponse.setCode(fe.getFireExtinguisherBrand().getCode());
      fireExtinguisherBrandResponse.setId(fe.getFireExtinguisherBrand().getId());

      fireExtinguisherTypeResponse.setName(fe.getFireExtinguisherType().getName());
      fireExtinguisherTypeResponse.setCode(fe.getFireExtinguisherType().getCode());
      fireExtinguisherTypeResponse.setId(fe.getFireExtinguisherType().getId());

      fireExtinguisherResponse.setUUID(fe.getUUID());
      fireExtinguisherResponse.setWarrantyDate(fe.getWarrantyDate());
      fireExtinguisherResponse.setFireExtinguisherType(fireExtinguisherTypeResponse);
      fireExtinguisherResponse.setFireExtinguisherBrand(fireExtinguisherBrandResponse);
      fireExtinguisherResponse.setModel(fe.getModel());
      fireExtinguisherResponse.setSerialNumber(fe.getSerialNumber());
      fireExtinguisherResponse.setCapacityQuantity(fe.getCapacityQuantity());
      fireExtinguisherResponse.setCapacityUnit(fe.getCapacityUnit());
      fireExtinguisherResponse.setManufactoringDate(fe.getManufactoringDate());
      fireExtinguisherResponse.setLastHydrostaticTest(fe.getLastHydrostaticTest());

      fireExtinguisherListResponse.add(fireExtinguisherResponse);
    }

    pageResponse.setPageSize(pageSize);
    pageResponse.setPageNumber(pageNumber);
    pageResponse.setFireExtinguisherList(fireExtinguisherListResponse);
    pageResponse.setPageTotal(fireExtinguisherPage.getTotalPages());

    return pageResponse;
  }

}
