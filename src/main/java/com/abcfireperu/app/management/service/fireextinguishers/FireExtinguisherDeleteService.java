package com.abcfireperu.app.management.service.fireextinguishers;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisher;
import com.abcfireperu.app.management.repository.FireExtinguisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherDeleteService {

  @Autowired
  private FireExtinguisherRepository fireExtinguisherRepository;

  public String deleteFireExtinguisher(Long Id) throws ResourceNotFoundExceptionClass {
    FireExtinguisher fireExtinguisher = fireExtinguisherRepository.findById(Id)
        .orElseThrow(() -> new ResourceNotFoundExceptionClass(
            "FireExtinguisher not found for this id :: " + Id));

    fireExtinguisherRepository.delete(fireExtinguisher);
    return "Ready";
  }
}
