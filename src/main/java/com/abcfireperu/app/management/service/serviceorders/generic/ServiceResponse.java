package com.abcfireperu.app.management.service.serviceorders.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceResponse {

  private Long id;
  private String code;
  private String name;

}
