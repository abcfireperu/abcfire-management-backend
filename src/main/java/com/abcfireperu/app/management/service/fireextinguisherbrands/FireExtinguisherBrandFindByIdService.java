package com.abcfireperu.app.management.service.fireextinguisherbrands;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisherBrand;
import com.abcfireperu.app.management.repository.FireExtinguisherBrandRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherBrandFindByIdService {

  @Autowired
  private FireExtinguisherBrandRepository fireExtinguisherBrandRepository;

  public Optional<FireExtinguisherBrand> getFireExtinguisherBrandById(Long Id)
      throws ResourceNotFoundExceptionClass {
    Optional<FireExtinguisherBrand> fireExtinguisherBrand = Optional
        .ofNullable(fireExtinguisherBrandRepository.findById(Id)
            .orElseThrow(() -> new ResourceNotFoundExceptionClass(
                "FireExtinguisherBrand not found for this id :: " + Id)));
    return fireExtinguisherBrand;
  }
}
