package com.abcfireperu.app.management.service.fireextinguishers.generic;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FireExtinguisherRequest {

  private String UUID;
  private Date warrantyDate;
  private Long brandId;
  private String model;
  private String serialNumber;
  private Long typeId;
  private float capacityQuantity;
  private String capacityUnit;
  private Date manufactoringDate;
  private Date lastHydrostaticTest;

  public FireExtinguisherRequest() {
  }
}
