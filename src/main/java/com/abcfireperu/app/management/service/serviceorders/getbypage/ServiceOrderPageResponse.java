package com.abcfireperu.app.management.service.serviceorders.getbypage;

import com.abcfireperu.app.management.service.serviceorders.generic.ServiceOrderResponse;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceOrderPageResponse {

  private int pageNumber;
  private int pageSize;
  private int pageTotal;
  private List<ServiceOrderResponse> serviceOrderResponseList;

}
