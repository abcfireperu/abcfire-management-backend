package com.abcfireperu.app.management.service.fireextinguishers;

import com.abcfireperu.app.management.model.FireExtinguisher;
import com.abcfireperu.app.management.repository.FireExtinguisherRepository;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherBrandResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherTypeResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FireExtinguisherFindAllService {

  @Autowired
  FireExtinguisherRepository fireExtinguisherRepository;

  public List<FireExtinguisherResponse> getAllFireExtinguishers() {

    List<FireExtinguisherResponse> fireExtinguisherListResponse = new ArrayList<>();
    List<FireExtinguisher> fireExtinguisherList = fireExtinguisherRepository.findAll();

    for (FireExtinguisher fe : fireExtinguisherList) {
      FireExtinguisherResponse fireExtinguisherResponse = new FireExtinguisherResponse();
      FireExtinguisherBrandResponse fireExtinguisherBrandResponse = new FireExtinguisherBrandResponse();
      FireExtinguisherTypeResponse fireExtinguisherTypeResponse = new FireExtinguisherTypeResponse();

      fireExtinguisherBrandResponse.setName(fe.getFireExtinguisherBrand().getName());
      fireExtinguisherBrandResponse.setCode(fe.getFireExtinguisherBrand().getCode());
      fireExtinguisherBrandResponse.setId(fe.getFireExtinguisherBrand().getId());

      fireExtinguisherTypeResponse.setName(fe.getFireExtinguisherType().getName());
      fireExtinguisherTypeResponse.setCode(fe.getFireExtinguisherType().getCode());
      fireExtinguisherTypeResponse.setId(fe.getFireExtinguisherType().getId());

      fireExtinguisherResponse.setUUID(fe.getUUID());
      fireExtinguisherResponse.setWarrantyDate(fe.getWarrantyDate());
      fireExtinguisherResponse.setFireExtinguisherType(fireExtinguisherTypeResponse);
      fireExtinguisherResponse.setFireExtinguisherBrand(fireExtinguisherBrandResponse);
      fireExtinguisherResponse.setModel(fe.getModel());
      fireExtinguisherResponse.setSerialNumber(fe.getSerialNumber());
      fireExtinguisherResponse.setCapacityQuantity(fe.getCapacityQuantity());
      fireExtinguisherResponse.setCapacityUnit(fe.getCapacityUnit());
      fireExtinguisherResponse.setManufactoringDate(fe.getManufactoringDate());
      fireExtinguisherResponse.setLastHydrostaticTest(fe.getLastHydrostaticTest());

      fireExtinguisherListResponse.add(fireExtinguisherResponse);
    }

    return fireExtinguisherListResponse;

  }


}
