package com.abcfireperu.app.management.service.sparepart.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SparePartRequest {

  private String code;
  private String name;

}
