package com.abcfireperu.app.management.service.roles;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Role;
import com.abcfireperu.app.management.repository.RoleRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolesFindByCodeService {

  @Autowired
  private RoleRepository roleRepository;

  public Optional<Role> getRoleByCode(String code)
      throws ResourceNotFoundExceptionClass {
    Optional<Role> role = Optional.ofNullable(roleRepository.findByCode(code)
        .orElseThrow(
            () -> new ResourceNotFoundExceptionClass("Role not found by this id ::" + code)));
    return role;
  }


}
