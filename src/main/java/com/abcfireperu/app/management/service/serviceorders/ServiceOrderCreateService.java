package com.abcfireperu.app.management.service.serviceorders;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Client;
import com.abcfireperu.app.management.model.ServiceOrder;
import com.abcfireperu.app.management.repository.ClientRepository;
import com.abcfireperu.app.management.repository.ServiceOrderRepository;
import com.abcfireperu.app.management.service.serviceorders.create.ServiceOrderClientResponse;
import com.abcfireperu.app.management.service.serviceorders.create.ServiceOrderCreateRequest;
import com.abcfireperu.app.management.service.serviceorders.create.ServiceOrderCreateResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceOrderCreateService {

  @Autowired
  private ServiceOrderRepository serviceOrderRepository;

  @Autowired
  private ClientRepository clientRepository;

  public ServiceOrderCreateResponse createServiceOrder(
      ServiceOrderCreateRequest serviceOrderCreateRequest) throws ResourceNotFoundExceptionClass {

    ServiceOrderCreateResponse serviceOrderCreateResponse = new ServiceOrderCreateResponse();
    ServiceOrder serviceOrder = new ServiceOrder();
    List<String> serviceOrderCodeList = new ArrayList<>();
    ServiceOrderClientResponse serviceOrderClientResponse = new ServiceOrderClientResponse();

    Optional<Client> clientOptional = clientRepository
        .findById(serviceOrderCreateRequest.getIdClient());
    if (clientOptional == null) {
      throw new ResourceNotFoundExceptionClass(
          "FireExtinguisherBrand not found for this id :: " + serviceOrderCreateRequest
              .getIdClient());
    }

    if (serviceOrderCreateRequest.getCode() != null) {
      serviceOrder.setCode(serviceOrderCreateRequest.getCode());
      serviceOrderCreateResponse.setCode(serviceOrderCreateRequest.getCode());

    } else {
      LocalDate localDate = LocalDate.now();
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
      String text = localDate.format(formatter);
      List<ServiceOrder> results = serviceOrderRepository.findByCodeStartsWith(text);
      if (results == null) {
        serviceOrder.setCode(text + "01");
        serviceOrderCreateResponse.setCode(text + "01");
      } else {
        /*
        for (ServiceOrder s : results) {
          serviceOrderCodeList.add(s.getCode());
        }
        Collections.sort(serviceOrderCodeList);
        String lastCode = serviceOrderCodeList.get(serviceOrderCodeList.size() - 1);
        */
        Integer size = serviceOrderCodeList.size();
        serviceOrder.setCode(text + "0" + size.toString());
        serviceOrderCreateResponse.setCode(text + "0" + size.toString());
      }


    }

    serviceOrder.setClient(clientOptional.get());
    serviceOrderRepository.save(serviceOrder);

    serviceOrderClientResponse.setName(clientOptional.get().getName());
    serviceOrderClientResponse.setAddress(clientOptional.get().getAddress());
    serviceOrderClientResponse.setCode(clientOptional.get().getCode());
    serviceOrderClientResponse.setId(clientOptional.get().getId());
    serviceOrderClientResponse.setContact(clientOptional.get().getContact());
    serviceOrderClientResponse.setCountry(clientOptional.get().getCountry());
    serviceOrderClientResponse.setDocumentType(clientOptional.get().getDocumentType());
    serviceOrderClientResponse.setManager(clientOptional.get().getManager());
    serviceOrderClientResponse.setMail(clientOptional.get().getMail());
    serviceOrderClientResponse.setRuc(clientOptional.get().getRuc());
    serviceOrderClientResponse.setPhoneNumber(clientOptional.get().getPhoneNumber());

    serviceOrderCreateResponse.setId(serviceOrder.getId());
    serviceOrderCreateResponse.setClient(serviceOrderClientResponse);

    return serviceOrderCreateResponse;

  }

}
