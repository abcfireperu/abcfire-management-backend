package com.abcfireperu.app.management.service.service;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.ServiceModel;
import com.abcfireperu.app.management.repository.ServiceRepository;
import com.abcfireperu.app.management.service.service.generic.ServiceRequest;
import com.abcfireperu.app.management.service.service.generic.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceCreateService {

  @Autowired
  private ServiceRepository serviceRepository;

  public ServiceResponse createService(ServiceRequest serviceRequest)
      throws ResourceNotFoundExceptionClass {

    ServiceModel service = new ServiceModel();
    ServiceResponse serviceResponse = new ServiceResponse();

    service.setCode(serviceRequest.getCode());
    service.setName(serviceRequest.getName());
    serviceRepository.save(service);

    serviceResponse.setId(service.getId());
    serviceResponse.setCode(service.getCode());
    serviceResponse.setName(service.getName());

    return serviceResponse;
  }
}
