package com.abcfireperu.app.management.service.sparepart;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.SparePart;
import com.abcfireperu.app.management.repository.SparePartRepository;
import com.abcfireperu.app.management.service.sparepart.generic.SparePartRequest;
import com.abcfireperu.app.management.service.sparepart.generic.SparePartResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SparePartCreateService {

  @Autowired
  private SparePartRepository sparePartRepository;

  public SparePartResponse createSparePart(SparePartRequest sparePartRequest)
      throws ResourceNotFoundExceptionClass {

    SparePart sparePart = new SparePart();
    SparePartResponse sparePartResponse = new SparePartResponse();

    sparePart.setCode(sparePartRequest.getCode());
    sparePart.setName(sparePartRequest.getName());
    sparePartRepository.save(sparePart);

    sparePartResponse.setId(sparePart.getId());
    sparePartResponse.setCode(sparePart.getCode());
    sparePartResponse.setName(sparePart.getName());

    return sparePartResponse;
  }
}
