package com.abcfireperu.app.management.service.clients;

import com.abcfireperu.app.management.model.Client;
import com.abcfireperu.app.management.model.ClientType;
import com.abcfireperu.app.management.model.Zone;
import com.abcfireperu.app.management.repository.ClientRepository;
import com.abcfireperu.app.management.service.clients.generic.ClientResponse;
import com.abcfireperu.app.management.service.clients.generic.ZoneResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientFindAllService {

  @Autowired
  private ClientRepository clientRepository;

  public List<ClientResponse> getAllClient() {

    List<ClientResponse> clientListResponse = new ArrayList<>();
    List<Client> clientList = clientRepository.findAll();

    for (Client c : clientList) {
      ClientResponse clientResponse = new ClientResponse();
      List<ZoneResponse> zoneResponseList = new ArrayList<>();

      for (Zone z : c.getZones()) {
        ZoneResponse zoneResponse = new ZoneResponse();
        zoneResponse.setCode(z.getCode());
        zoneResponse.setName(z.getName());
        zoneResponse.setId(z.getId());
        zoneResponseList.add(zoneResponse);
      }

      clientResponse.setAddress(c.getAddress());
      clientResponse.setCode(c.getCode());
      clientResponse.setContact(c.getContact());
      clientResponse.setCountry(c.getCountry());
      clientResponse.setId(c.getId());
      clientResponse.setName(c.getName());
      clientResponse.setDocumentType(c.getDocumentType());
      clientResponse.setManager(c.getManager());
      clientResponse.setMail(c.getMail());
      clientResponse.setRuc(c.getRuc());
      clientResponse.setZones(zoneResponseList);
      clientResponse.setPhoneNumber(c.getPhoneNumber());

      if (c.getType() == ClientType.NATURAL) {
        clientResponse.setType("NATURAL");
      } else {
        clientResponse.setType("JURIDICA");
      }

      clientListResponse.add(clientResponse);

    }

    return clientListResponse;
  }
}

