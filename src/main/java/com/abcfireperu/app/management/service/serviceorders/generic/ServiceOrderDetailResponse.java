package com.abcfireperu.app.management.service.serviceorders.generic;

import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceOrderDetailResponse {

  private Long id;
  private ServiceResponse service;
  private FireExtinguisherResponse fireExtinguisher;
  private Date hydrostaticTest;
  private Date lastMaintenance;
}
