package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.service.ubications.UbicationFindByNameAndTypeService;
import com.abcfireperu.app.management.service.ubications.generic.UbicationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/ubication")
public class UbicationController {

  @Autowired
  private UbicationFindByNameAndTypeService ubicationFindByNameAndTypeService;

  @GetMapping("/")
  public ResponseEntity<UbicationResponse> getPageById(@RequestParam String name,
      @RequestParam String type)
      throws ResourceNotFoundExceptionClass {
    UbicationResponse ubicationResponse = ubicationFindByNameAndTypeService
        .getUbicationByNameAndType(name, type);
    return ResponseEntity.ok().body(ubicationResponse);
  }


}
