package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.Role;
import com.abcfireperu.app.management.service.roles.RolesFindByCodeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/roles")
public class RoleController {

  @Autowired
  private RolesFindByCodeService rolesFindByCodeService;

  @Operation(summary = "Get a role with features by its cde")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Found the role",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = Role.class))}),
      @ApiResponse(responseCode = "404", description = "Role not found with this code",
          content = @Content)})

  @GetMapping("/")
  @ResponseBody

  public ResponseEntity<Role> getRoleByCode(@Parameter(description = "id of book to be searched")
  @RequestParam String code) throws ResourceNotFoundExceptionClass {
    Role role = rolesFindByCodeService.getRoleByCode(code).get();
    return ResponseEntity.ok().body(role);
  }
}
