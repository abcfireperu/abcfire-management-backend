package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisherBrand;
import com.abcfireperu.app.management.service.fireextinguisherbrands.FireExtinguisherBrandFindByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/fireextinguisherbrand")
public class FireExtinguisherBrandController {

  @Autowired
  private FireExtinguisherBrandFindByIdService fireExtinguisherBrandFindByIdService;

  @GetMapping("/")
  @ResponseBody
  public ResponseEntity<FireExtinguisherBrand> getFireExtinguisherBrandById(
      @RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {
    FireExtinguisherBrand fireExtinguisherBrand = fireExtinguisherBrandFindByIdService
        .getFireExtinguisherBrandById(Id).get();
    return ResponseEntity.ok().body(fireExtinguisherBrand);
  }

}
