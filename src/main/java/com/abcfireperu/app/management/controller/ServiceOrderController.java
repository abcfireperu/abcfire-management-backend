package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.service.serviceorders.ServiceOrderCreateService;
import com.abcfireperu.app.management.service.serviceorders.ServiceOrderFindByIdService;
import com.abcfireperu.app.management.service.serviceorders.ServiceOrderFindByPageService;
import com.abcfireperu.app.management.service.serviceorders.create.ServiceOrderCreateRequest;
import com.abcfireperu.app.management.service.serviceorders.create.ServiceOrderCreateResponse;
import com.abcfireperu.app.management.service.serviceorders.generic.ServiceOrderResponse;
import com.abcfireperu.app.management.service.serviceorders.getbypage.ServiceOrderPageResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/serviceorder")
public class ServiceOrderController {

  @Autowired
  ServiceOrderFindByPageService serviceOrderFindByPageService;

  @Autowired
  ServiceOrderFindByIdService serviceOrderFindByIdService;

  @Autowired
  ServiceOrderCreateService serviceOrderCreateService;

  @GetMapping("/page")
  public ServiceOrderPageResponse getByPage(@RequestParam int pageNumber,
      @RequestParam int pageSize) {
    return serviceOrderFindByPageService.getServiceOrderByPage(pageNumber, pageSize);
  }

  @GetMapping("/")
  //listo
  public ResponseEntity<ServiceOrderResponse> getServiceOrderById(@RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {
    ServiceOrderResponse serviceOrderResponse = serviceOrderFindByIdService.getServiceOrderById(Id);
    return ResponseEntity.ok().body(serviceOrderResponse);
  }

  @PostMapping("/")
  public ServiceOrderCreateResponse createServiceOrder(
      @Valid @RequestBody ServiceOrderCreateRequest serviceOrderCreateRequest)
      throws ResourceNotFoundExceptionClass {
    ServiceOrderCreateResponse serviceOrderResponse = serviceOrderCreateService
        .createServiceOrder(serviceOrderCreateRequest);
    return serviceOrderResponse;
  }

}
