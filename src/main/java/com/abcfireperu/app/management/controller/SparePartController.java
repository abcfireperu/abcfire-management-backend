package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.service.sparepart.SparePartCreateService;
import com.abcfireperu.app.management.service.sparepart.SparePartDeleteService;
import com.abcfireperu.app.management.service.sparepart.generic.SparePartRequest;
import com.abcfireperu.app.management.service.sparepart.generic.SparePartResponse;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SparePartController {

  @Autowired
  private SparePartCreateService sparePartCreateService;

  @Autowired
  private SparePartDeleteService sparePartDeleteService;

  @PostMapping("/")
  public SparePartResponse createSparePart(@Valid @RequestBody SparePartRequest sparePartRequest)
      throws ResourceNotFoundExceptionClass {
    SparePartResponse spartPartResponse = sparePartCreateService.createSparePart(sparePartRequest);
    return spartPartResponse;
  }

  @DeleteMapping("/")
  public Map<String, Boolean> deleteFireExtinguisher(@RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {

    String res = sparePartDeleteService.deleteSparePart(Id);

    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }

}
