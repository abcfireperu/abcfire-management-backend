package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.service.service.ServiceCreateService;
import com.abcfireperu.app.management.service.service.ServiceDeleteService;
import com.abcfireperu.app.management.service.service.generic.ServiceRequest;
import com.abcfireperu.app.management.service.service.generic.ServiceResponse;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class ServiceController {

  @Autowired
  private ServiceCreateService serviceCreateService;

  @Autowired
  private ServiceDeleteService serviceDeleteService;

  @PostMapping("/")
  public ServiceResponse createClient(@Valid @RequestBody ServiceRequest serviceRequest)
      throws ResourceNotFoundExceptionClass {
    ServiceResponse serviceResponse = serviceCreateService.createService(serviceRequest);
    return serviceResponse;
  }

  @DeleteMapping("/")
  public Map<String, Boolean> deleteFireExtinguisher(@RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {

    String res = serviceDeleteService.deleteService(Id);

    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }

}
