package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.service.clients.ClientCreateService;
import com.abcfireperu.app.management.service.clients.ClientDeleteService;
import com.abcfireperu.app.management.service.clients.ClientFindAllService;
import com.abcfireperu.app.management.service.clients.ClientFindByIdService;
import com.abcfireperu.app.management.service.clients.ClientFindByPageService;
import com.abcfireperu.app.management.service.clients.ClientUpdateService;
import com.abcfireperu.app.management.service.clients.generic.ClientRequest;
import com.abcfireperu.app.management.service.clients.generic.ClientResponse;
import com.abcfireperu.app.management.service.clients.getbypage.ClientPageResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/client")
public class ClientController {

  @Autowired
  private ClientFindAllService clientFindAllService;

  @Autowired
  private ClientFindByPageService clientFindByPageService;

  @Autowired
  private ClientFindByIdService clientFindByIdService;

  @Autowired
  private ClientCreateService clientCreateService;

  @Autowired
  private ClientUpdateService clientUpdateService;

  @Autowired
  private ClientDeleteService clientDeleteService;

  @GetMapping("/all")
  public List<ClientResponse> getAllClient() {
    return clientFindAllService.getAllClient();
  }

  @GetMapping("/page")
  public ClientPageResponse getByPage(@RequestParam int pageNumber, @RequestParam int pageSize) {
    return clientFindByPageService.getClientByPage(pageNumber, pageSize);
  }

  @GetMapping("/")
  public ResponseEntity<ClientResponse> getClientById(@RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {
    ClientResponse clientResponse = clientFindByIdService.getClientById(Id);
    return ResponseEntity.ok().body(clientResponse);
  }

  @PostMapping("/")
  public ClientResponse createClient(@Valid @RequestBody ClientRequest clientRequest)
      throws ResourceNotFoundExceptionClass {
    ClientResponse clientResponse = clientCreateService.createClient(clientRequest);
    return clientResponse;
  }

  @PutMapping("/")
  public ResponseEntity<ClientResponse> updateClient(@RequestParam Long Id,
      @Valid @RequestBody ClientRequest clientRequest) throws ResourceNotFoundExceptionClass {
    ClientResponse clientResponse = clientUpdateService.updateClient(Id, clientRequest);

    return ResponseEntity.ok().body(clientResponse);
  }

  @DeleteMapping("/")
  public Map<String, Boolean> deleteClient(@RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {

    String res = clientDeleteService.deleteClient(Id);

    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }

}
