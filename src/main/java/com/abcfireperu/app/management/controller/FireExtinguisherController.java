package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.service.fireextinguishers.FireExtinguisherCreateService;
import com.abcfireperu.app.management.service.fireextinguishers.FireExtinguisherDeleteService;
import com.abcfireperu.app.management.service.fireextinguishers.FireExtinguisherFindAllService;
import com.abcfireperu.app.management.service.fireextinguishers.FireExtinguisherFindByIdService;
import com.abcfireperu.app.management.service.fireextinguishers.FireExtinguisherFindByPageService;
import com.abcfireperu.app.management.service.fireextinguishers.FireExtinguisherUpdateService;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherRequest;
import com.abcfireperu.app.management.service.fireextinguishers.generic.FireExtinguisherResponse;
import com.abcfireperu.app.management.service.fireextinguishers.getbypage.FireExtinguisherPageResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/fireextinguisher")
public class FireExtinguisherController {

  @Autowired
  private FireExtinguisherCreateService fireExtinguisherCreateService;

  @Autowired
  private FireExtinguisherFindAllService fireExtinguisherFindAllService;

  @Autowired
  private FireExtinguisherFindByIdService fireExtinguisherFindByIdService;

  @Autowired
  private FireExtinguisherUpdateService fireExtinguisherUpdateService;

  @Autowired
  private FireExtinguisherDeleteService fireExtinguisherDeleteService;

  @Autowired
  private FireExtinguisherFindByPageService fireExtinguisherFindByPage;


  @GetMapping("/all")
  public List<FireExtinguisherResponse> getAllFireExtinguisher() {
    return fireExtinguisherFindAllService.getAllFireExtinguishers();
  }

  @GetMapping("/page")
  public FireExtinguisherPageResponse getFireExtinguisherByPage(@RequestParam int pageSize,
      @RequestParam int pageNumber) {
    return fireExtinguisherFindByPage.getFireExtinguisherByPage(pageNumber, pageSize);
  }

  @GetMapping("/")
  public ResponseEntity<FireExtinguisherResponse> getFireExtinguisherById(
      @RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {
    FireExtinguisherResponse fireExtinguisherResponse = fireExtinguisherFindByIdService
        .getFireExtinguisherById(Id);
    return ResponseEntity.ok().body(fireExtinguisherResponse);
  }


  @PostMapping("/")
  public FireExtinguisherResponse createFireExtinguisher(
      @Valid @RequestBody FireExtinguisherRequest fireExtinguisherRequest)
      throws ResourceNotFoundExceptionClass {

    FireExtinguisherResponse fireExtinguisherResponse = fireExtinguisherCreateService
        .createFireExtinguisher(fireExtinguisherRequest);
    return fireExtinguisherResponse;
  }

  @PutMapping("/")
  public ResponseEntity<FireExtinguisherResponse> updateFireExtinguisher(@RequestParam Long Id,
      @Valid @RequestBody FireExtinguisherRequest fireExtinguisherRequest)
      throws ResourceNotFoundExceptionClass {

    FireExtinguisherResponse fireExtinguisherResponse = fireExtinguisherUpdateService
        .updateFireExtinguisher(Id, fireExtinguisherRequest);

    return ResponseEntity.ok().body(fireExtinguisherResponse);
  }

  @DeleteMapping("/")
  public Map<String, Boolean> deleteFireExtinguisher(@RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {

    String res = fireExtinguisherDeleteService.deleteFireExtinguisher(Id);

    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }

}
