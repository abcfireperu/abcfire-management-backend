package com.abcfireperu.app.management.controller;

import com.abcfireperu.app.management.exception.ResourceNotFoundExceptionClass;
import com.abcfireperu.app.management.model.FireExtinguisherType;
import com.abcfireperu.app.management.service.fireextinguishertypes.FireExtinguisherTypeFindByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/fireextinguishertype")
public class FireExtinguisherTypeController {

  @Autowired
  private FireExtinguisherTypeFindByIdService fireExtinguisherTypeFindByIdService;

  @GetMapping("/")
  @ResponseBody
  public ResponseEntity<FireExtinguisherType> getFireExtinguisherTypeById(
      @RequestParam Long Id)
      throws ResourceNotFoundExceptionClass {
    FireExtinguisherType fireExtinguisherType = fireExtinguisherTypeFindByIdService
        .getFireExtinguisherTypeById(Id).get();
    return ResponseEntity.ok().body(fireExtinguisherType);
  }

}
