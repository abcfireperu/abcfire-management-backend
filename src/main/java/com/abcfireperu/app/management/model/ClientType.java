package com.abcfireperu.app.management.model;

public enum ClientType {
  NATURAL,
  JURIDICA
}
