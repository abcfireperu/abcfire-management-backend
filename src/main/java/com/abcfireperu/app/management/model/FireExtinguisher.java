package com.abcfireperu.app.management.model;

import com.abcfireperu.app.management.model.audit.DateAudit;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "fire_extinguishers")
public class FireExtinguisher extends DateAudit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "UUID")
  private String UUID;
  @Column(name = "warranty_date")
  @Temporal(TemporalType.DATE)
  private Date warrantyDate;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "id_brand", nullable = false)
  private FireExtinguisherBrand fireExtinguisherBrand;

  @Column(name = "model")
  private String model;
  @Column(name = "serial_number")
  private String serialNumber;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "id_type", nullable = false)
  private FireExtinguisherType fireExtinguisherType;

  @Column(name = "capacity_quantity")
  private float capacityQuantity;
  @Column(name = "capacity_unity")
  private String capacityUnit;
  @Column(name = "manufactoring_date")
  @Temporal(TemporalType.DATE)
  private Date manufactoringDate;
  @Column(name = "last_hydrostatic_test")
  @Temporal(TemporalType.DATE)
  private Date lastHydrostaticTest;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "fire_extinguishers_spare_parts", joinColumns = @JoinColumn(name = "fire_extinguishers_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "spare_parts_id", referencedColumnName = "id"))
  private Set<SparePart> spareParts;

  @OneToMany(mappedBy = "fireExtinguisher", fetch = FetchType.LAZY)
  private Set<ServiceOrderDetail> serviceOrderDetails;

  @OneToMany(mappedBy = "fireExtinguisher", fetch = FetchType.LAZY)
  private Set<ServiceOrderFireExtinguisher> serviceOrderFireExtinguishers;

  public FireExtinguisher() {
  }
}
