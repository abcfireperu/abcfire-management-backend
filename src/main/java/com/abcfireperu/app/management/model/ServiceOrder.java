package com.abcfireperu.app.management.model;

import com.abcfireperu.app.management.model.audit.DateAudit;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "service_orders")
public class ServiceOrder extends DateAudit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "code")
  private String code;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id_client")
  private Client client;

  @OneToMany(mappedBy = "serviceOrder", fetch = FetchType.LAZY)
  private Set<ServiceOrderDetail> serviceOrderDetails;
  @Column(name = "status")
  private String status;

  @OneToMany(mappedBy = "serviceOrder", fetch = FetchType.LAZY)
  private Set<ServiceOrderFireExtinguisher> serviceOrderFireExtinguishers;

}
