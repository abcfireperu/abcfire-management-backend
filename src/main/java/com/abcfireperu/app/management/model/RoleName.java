package com.abcfireperu.app.management.model;

public enum RoleName {
  ROLE_USER,
  ROLE_ADMIN
}
