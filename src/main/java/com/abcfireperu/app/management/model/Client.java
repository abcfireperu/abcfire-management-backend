package com.abcfireperu.app.management.model;

import com.abcfireperu.app.management.model.audit.DateAudit;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "clients")
public class Client extends DateAudit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "code")
  private String code;
  @Column(name = "name")
  private String name;
  @Column(name = "type")
  @Enumerated(EnumType.STRING)
  private ClientType type;
  @Column(name = "document_type")
  private String documentType;
  @Column(name = "ruc", unique = true)
  private String ruc;
  @Column(name = "address")
  private String address;
  @Column(name = "country")
  private String country;
  @Column(name = "phone_number")
  private String phoneNumber;
  @Column(name = "mail")
  private String mail;
  @Column(name = "contact")
  private String contact;
  @Column(name = "manager")
  private String manager;
  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(name = "client_zone", joinColumns = @JoinColumn(name = "client_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "zone_id", referencedColumnName = "id"))
  private Set<Zone> zones = new HashSet<>();
}
