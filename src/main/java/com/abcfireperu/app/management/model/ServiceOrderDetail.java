package com.abcfireperu.app.management.model;

import com.abcfireperu.app.management.model.audit.DateAudit;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "service_orders_details")
public class ServiceOrderDetail extends DateAudit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "service_order_id", nullable = false)
  private ServiceOrder serviceOrder;
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "service_id", nullable = false)
  private ServiceModel serviceModel;
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "fire_extingisher_id", nullable = false)
  private FireExtinguisher fireExtinguisher;
  @Column(name = "hydrostatic_test")
  @Temporal(TemporalType.DATE)
  private Date hydrostaticTest;
  @Column(name = "last_maintenance")
  @Temporal(TemporalType.DATE)
  private Date lastMaintenance;

}
