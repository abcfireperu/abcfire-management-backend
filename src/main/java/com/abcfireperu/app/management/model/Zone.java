package com.abcfireperu.app.management.model;

import com.abcfireperu.app.management.model.audit.DateAudit;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "zones")
public class Zone extends DateAudit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "name")
  private String name;
  @Column(name = "code")
  private String code;
  @ManyToMany(mappedBy = "zones")
  private Set<Client> clients = new HashSet<>();
}
