package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.FireExtinguisherBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FireExtinguisherBrandRepository extends
    JpaRepository<FireExtinguisherBrand, Long> {

}
