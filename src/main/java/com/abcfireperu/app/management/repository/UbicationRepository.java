package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.Ubication;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UbicationRepository extends JpaRepository<Ubication, Long> {

  Optional<Ubication> findByNameAndType(String name, String type);
}
