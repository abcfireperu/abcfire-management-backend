package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.FireExtinguisherType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FireExtinguisherTypeRepository extends JpaRepository<FireExtinguisherType, Long> {

}
