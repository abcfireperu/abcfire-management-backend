package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.FireExtinguisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FireExtinguisherRepository extends JpaRepository<FireExtinguisher, Long> {

}
