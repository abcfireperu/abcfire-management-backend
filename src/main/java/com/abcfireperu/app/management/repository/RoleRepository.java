package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.Role;
import com.abcfireperu.app.management.model.RoleName;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByCode(String code);

  Optional<Role> findByName(RoleName roleName);
}
