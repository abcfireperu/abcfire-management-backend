package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.ServiceOrder;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceOrderRepository extends JpaRepository<ServiceOrder, Long> {

  List<ServiceOrder> findByCodeStartsWith(String code);

}
