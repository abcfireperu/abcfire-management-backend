package com.abcfireperu.app.management.repository;

import com.abcfireperu.app.management.model.ServiceOrderFireExtinguisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceOrderFireExtinguisherRepository extends
    JpaRepository<ServiceOrderFireExtinguisher, Long> {

}
